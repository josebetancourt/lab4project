package geometry;

public class Circle implements Shape {
	private double radius;
	private double area;
	private double perimeter;
	
	public Circle(double radius) {
		this.radius = radius;
	}
	
	public double getRadius() {
		return this.radius;
	}
	
	public double getArea() {
		double area = Math.PI * Math.pow(this.radius, 2);
		this.area = area;
		return this.area;
	}
	
	public double getPerimeter() {
		double perimeter = 2* Math.PI * this.radius;
		this.perimeter = perimeter;
		return this.perimeter;
	}
	
	
	
}
