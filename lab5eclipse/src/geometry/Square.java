//Jose Carlos Betancourt Saiz de la Mora #1935788

package geometry;

public class Square extends Rectangle {
	
	public Square(double sideLength) {
		super(sideLength, sideLength);
	}
	
}
