//Jose Carlos Betancourt Saiz de la Mora #1935788

package geometry;

public class LotsOfShapes {

	public static void main(String[] args) {
		Shape[] shapes = new Shape[5];
		
		shapes[0] = new Rectangle(1,2);
		shapes[1] = new Rectangle(3,4);
		shapes[2] = new Circle(3);
		shapes[3] = new Circle(5);
		shapes[4] = new Square(2);
		
		for (int i=0; i<shapes.length; i++) {
			System.out.println(i + "- " + "Area: " + shapes[i].getArea() + " Perimeter: " + shapes[i].getPerimeter());
		}
		
	}

}
