//Jose Carlos Betancourt Saiz de la Mora #1935788

package geometry;

public class Rectangle implements Shape {
	private double length;
	private double width;
	private double area;
	private double perimeter;
	
	public Rectangle(double length, double width) {
		this.length = length;
		this.width = width;
	}
	
	public double getLenght() {
		return this.length;
	}
	
	public double getWidth() {
		return this.width;
	}
	
	public double getArea() {
		double area = this.length * this.width;
		this.area = area;
		return this.area;
	}
	
	public double getPerimeter() {
		double perimeter = 2 * (this.length + this.width);
		this.perimeter = perimeter;
		return this.perimeter;
	}
	
}
