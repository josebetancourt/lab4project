//Jose Carlos Betancourt Saiz de la Mora #1935788

package inheritance;

public class BookStore {

	public static void main(String[] args) {
		Book[] books = new Book[5];
		
		Book bookOne = new Book("BookOne", "AuthorOne");
		Book bookTwo = new Book("BookTwo", "AuthorTwo");
		Book ebookOne = new ElectronicBook("EBookOne", "EAuthorOne", 100000);
		Book ebookTwo = new ElectronicBook("EBookTwo", "EAuthorTwo", 200000);
		Book ebookThree = new ElectronicBook("EBookThree", "EAuthorThree", 300000);
		
		books[0] = bookOne;
		books[2] = bookTwo;
		books[1] = ebookOne;
		books[3] = ebookTwo;
		books[4] = ebookThree;
		
		for (int i=0; i<books.length; i++) {
			System.out.println(books[i]);
		}
		
	}

}
